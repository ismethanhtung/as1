using System;
using System.Collections.Generic;

class NguoiLaoDong
{
    public string HoTen { get; set; }
    public int NamSinh { get; set; }
    public double LuongCoBan { get; set; }

    public NguoiLaoDong(){}

    public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan){
        HoTen = hoTen;
        NamSinh = namSinh;
        LuongCoBan = luongCoBan;
    }

    public void NhapThongTin(string hoTen, int namSinh, double luongCoBan){
        HoTen = hoTen;
        NamSinh = namSinh;
        LuongCoBan = luongCoBan;
    }

    public virtual double TinhLuong(){
        return LuongCoBan;
    }

    public void XuatThongTin(){
        Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}.");
    }
}

class GiaoVien : NguoiLaoDong
{
    public double HeSoLuong { get; set; }

    public GiaoVien(){}

    public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong) : base(hoTen, namSinh, luongCoBan){
        HeSoLuong = heSoLuong;
    }

    public new void NhapThongTin(string hoTen, int namSinh, double luongCoBan){
        base.NhapThongTin(hoTen, namSinh, luongCoBan);
    }

    public void NhapThongTin(double heSoLuong){
        HeSoLuong = heSoLuong;
    }

    public override double TinhLuong(){
        return LuongCoBan * HeSoLuong * 1.25;
    }

    public new void XuatThongTin(){
        base.XuatThongTin();
        Console.WriteLine($"He so luong: {HeSoLuong}, Luong: {TinhLuong()}");
    }

    public void XuLy(){
        HeSoLuong += 0.6;
    }
}

class Program {
    static void Main(string[] args){
        try {
            Console.Write("Nhap so luong giao vien: ");
            int soLuong = Convert.ToInt32(Console.ReadLine());

            List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

            for (int i = 0; i < soLuong; i++){
                Console.WriteLine($"\nNhap thong tin giao vien {i + 1}:");
                Console.Write("Ho ten: ");
                string hoTen = Console.ReadLine();
                Console.Write("Nam sinh: ");
                int namSinh = Convert.ToInt32(Console.ReadLine());
                Console.Write("Luong co ban: ");
                double luongCoBan = Convert.ToDouble(Console.ReadLine());
                Console.Write("He so luong: ");
                double heSoLuong = Convert.ToDouble(Console.ReadLine());

                GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
                danhSachGiaoVien.Add(giaoVien);
            }

            Console.WriteLine("\nThong tin giao vien co luong thap nhat:");
            double luongThapNhat = double.MaxValue;
            GiaoVien giaoVienLuongThapNhat = null;

            foreach (GiaoVien giaoVien in danhSachGiaoVien){
                double luong = giaoVien.TinhLuong();
                if (luong < luongThapNhat){
                    luongThapNhat = luong;
                    giaoVienLuongThapNhat = giaoVien;
                }
            }

            if (giaoVienLuongThapNhat != null){
                giaoVienLuongThapNhat.XuatThongTin();
            }

            Console.WriteLine("\nNhan mot phim bat ky de dung chuong trinh");
            Console.ReadKey();
        }
        catch (Exception ex) {
            Console.WriteLine("Da xay ra loi: " + ex.Message);
            Console.WriteLine("Vi tri loi: " + ex.StackTrace);
        }
    }
}

